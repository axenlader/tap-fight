
extends KinematicBody2D

# member variables here, example:
# var a=2
# var b="textvar"
export var speed=100

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	set_fixed_process(true)
	
func _fixed_process(delta):
	move(Vector2(-sin(get_rot()),-cos(get_rot()))*-speed*delta)
	if is_colliding():
		if get_collider().is_in_group("Enemy"):
			get_collider().hurt(100)
		queue_free()
	if get_pos().y<-100:
		queue_free()
	


