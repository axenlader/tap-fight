
extends Node2D

# member variables here, example:
# var a=2
# var b="textvar"
var wave = 0
var best_wave = 0
var enemies = []
var lives = 20

var WAVE_OVER=0
var WAVE_RUNNING=1
var WAVE_WAIT = 2
var WAVE_READY = 3
var GAME_OVER = 4
var START_SCREEN = 5

var state = WAVE_READY
var clicking = false
var start_dragging
var stop_dragging
var time = 0
var wait_wave_until=0
var wait_delay = 5
var start_wait=0
var dragging_barrel = null
var money=0

var weapon = 1
var mouse_pos = Vector2(360,0)

var on_ui = false



var file_path = "user://score.bin"

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	randomize()
	load_score()
	weapon = 0
	if (best_wave > 0):
		var txt = str("Best #",best_wave)
		get_tree().get_root().get_node("Game/CanvasLayer/Label").set_text(txt)
	money = 0
	add_money(0)
	state = START_SCREEN
	get_node("StartCanvas/Sprite").show()
	get_node("StartCanvas/Label").show()
	get_node("CanvasLayer/NextWaveBar").hide()
	get_node("CanvasLayer/NextWaveText").hide()
	get_node("CanvasLayer/Lives").set_text(str(lives))
	set_fixed_process(true)
	set_process_input(true)

func reset():
	wave = 0
	lives=20
	enemies.clear()
	time = 0
	wait_wave_until = 0
	_ready()
	
func _fixed_process(delta):
	time = time+delta
	if state == WAVE_OVER:
		start_wait=time
		wait_wave_until = time+wait_delay
		state = WAVE_WAIT
	elif state == WAVE_WAIT:
		if time >= wait_wave_until:
			get_node("CanvasLayer/NextWaveBar").hide()
			get_node("CanvasLayer/NextWaveText").hide()
			state = WAVE_READY
		else:
			get_node("CanvasLayer/NextWaveBar").show()
			get_node("CanvasLayer/NextWaveText").show()
			get_node("CanvasLayer/NextWaveBar").set_value(100-100*(time-start_wait)/wait_delay)
	elif state == WAVE_READY:
		load_wave()
		state = WAVE_RUNNING
		
func add_money(m):
	money = money+m
	get_tree().get_root().get_node("Game/CanvasLayer/Money").set_text(str(money,"$"))
	
func load_wave():
	wave=wave+1
	var txt = str("Wave #",wave)
	if (best_wave > 0):
		txt = str(txt, " Best #",best_wave)
	get_tree().get_root().get_node("Game/CanvasLayer/Label").set_text(txt)
	var res = ResourceLoader.load("res://TankRed.scn")
	var res2 = ResourceLoader.load("res://TankBlue.scn")
	var res3 = ResourceLoader.load("res://TankGreen.scn")
	for i in range(wave):
		var tr
		var r = randf()
		if r<0.333:
			tr = res.instance()
		elif r<0.666:
			tr = res2.instance()
		else:
			tr = res3.instance()
		get_tree().get_root().get_node("Game/Wave").add_child(tr)
		tr.set_pos(Vector2(randf()*700+10,get_pos().y-randi()%500))
		enemies.append(tr)
		
func _on_enemy_succeed():
	if state==GAME_OVER||state == START_SCREEN:
		return
	lives=lives-1
	get_node("CanvasLayer/Lives").set_text(str(lives))
	if lives<=0:
		game_over()
func game_over():
	state = GAME_OVER
	if (wave > best_wave):
		best_wave = wave
		save_score()
	get_tree().get_root().get_node("Game").add_child(ResourceLoader.load("res://TryAgain.scn").instance())
	
func _on_died_enemy(enemy):
	if state==GAME_OVER||state == START_SCREEN:
		return
	add_money(int(10*(1280-enemy.get_pos().y)/1280))
	enemies.erase(enemy)
	if enemies.empty():
		state = WAVE_OVER

func _input(ev):
	if state==GAME_OVER:
		return
	if (ev.type==InputEvent.MOUSE_BUTTON):
		clicking = !clicking
		if clicking:
			pass
		else:
			if state == START_SCREEN:
				get_node("StartCanvas/Sprite").hide()
				get_node("StartCanvas/Label").hide()
				state = WAVE_READY
			elif ev.pos.y<1100:
				if dragging_barrel!=null:
					dragging_barrel=null
				else:
					fire(ev.pos)
					
	elif ev.type==InputEvent.MOUSE_MOTION:
		mouse_pos = ev.pos
		get_node("MyTank/Canon").look_at(ev.pos)
		if dragging_barrel!=null:
			dragging_barrel.set_pos(ev.pos)
	
func fire(pos):
	if (weapon==0):
		shot(pos)
	elif (weapon==1):
		shot2(pos)
	elif (weapon==2):
		shot3(pos)
func shot(pos2):
	var tr = ResourceLoader.load("res://Bullet.scn").instance()
	get_tree().get_root().add_child(tr)
	tr.set_pos(get_node("MyTank/Canon").get_global_pos())
	tr.look_at(pos2)
	
func shot2(pos2):
	var pos = get_node("MyTank/Canon").get_global_pos()
	var tr = ResourceLoader.load("res://Bullet.scn").instance()
	get_tree().get_root().add_child(tr)
	tr.set_pos(pos)
	tr.look_at(pos2)
	
	var tr = ResourceLoader.load("res://Bullet.scn").instance()
	get_tree().get_root().add_child(tr)
	tr.set_pos(pos)
	tr.look_at(Vector2(pos2.x-100,pos2.y))
	
	var tr = ResourceLoader.load("res://Bullet.scn").instance()
	get_tree().get_root().add_child(tr)
	tr.set_pos(pos)
	tr.look_at(Vector2(pos2.x+100,pos2.y))
	
func shot3(pos2):
	var pos = get_node("MyTank/Canon").get_global_pos()
	var tr = ResourceLoader.load("res://Bullet.scn").instance()
	get_tree().get_root().add_child(tr)
	tr.set_pos(pos)
	tr.look_at(pos2)
	
	var tr = ResourceLoader.load("res://Bullet.scn").instance()
	get_tree().get_root().add_child(tr)
	tr.set_pos(pos)
	tr.look_at(Vector2(pos2.x-100,pos2.y))
	
	var tr = ResourceLoader.load("res://Bullet.scn").instance()
	get_tree().get_root().add_child(tr)
	tr.set_pos(pos)
	tr.look_at(Vector2(pos2.x+100,pos2.y))
	
	var tr = ResourceLoader.load("res://Bullet.scn").instance()
	get_tree().get_root().add_child(tr)
	tr.set_pos(Vector2(pos.x,pos.y-100))
	tr.look_at(Vector2(pos2.x,pos2.y))
	

func _on_add_box_pressed():
	if (money>=10&&dragging_barrel==null):
		add_money(-10)
		var tr = ResourceLoader.load("res://Barrel.scn").instance()
		get_tree().get_root().add_child(tr)
		dragging_barrel = tr


func _on_buy_triple_weapon():
	if (weapon!=1&&money>=200):
		add_money(-200)
		weapon=1


func _on_ButtonWeapon2_pressed():
	if (weapon!=2&&money>=500):
		add_money(-500)
		weapon=2

func try_buy(amount):
	if (money>=amount):
		add_money(-amount)
		return true
	return false

func _on_buy_tower_pressed():
	if (money>=200&&dragging_barrel==null):
		add_money(-200)
		var tr = ResourceLoader.load("res://Tower.scn").instance()
		get_tree().get_root().add_child(tr)
		dragging_barrel = tr

func save_score():
	var file = File.new()
	file.open_encrypted_with_pass(file_path, File.WRITE, OS.get_unique_ID())
	file.store_var({"highscore":best_wave})
	file.close()
	pass
	
func load_score():
	var file = File.new()
	if file.file_exists(file_path):
		file.open_encrypted_with_pass(file_path, File.READ, OS.get_unique_ID())
		best_wave = file.get_var().highscore
		print(best_wave)
		file.close()