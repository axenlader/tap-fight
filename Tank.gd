
extends KinematicBody2D

# member variables here, example:
# var a=2
# var b="textvar"
export var speed = 100
var life = 100
export var def = 0
var time = 0
var next_fire=0
export var atk=10
var fire_reload=1

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	get_node("ProgressBar").set_value(life)
	set_fixed_process(true)
	
func _fixed_process(delta):
	time = time+delta
#	move(Vector2(0,speed*delta))
	
	move(Vector2(-sin(get_node("Shape").get_rot()),-cos(get_node("Shape").get_rot()))*-speed*delta)
	if (get_pos().y>1350):
		get_tree().get_root().get_node("Game")._on_enemy_succeed()
		die()
	else:
		if is_colliding():
			if get_collider().is_in_group("Obstacle"):
				get_node("Shape").look_at(get_collider().get_pos())
				fire_obstacle(get_collider())
		else:
			get_node("Shape").look_at(Vector2(get_pos().x,1400))
			
func fire_obstacle(obj):
	if (time >= next_fire):
		obj.hurt(atk)
		next_fire=time+fire_reload

func die():
	get_tree().get_root().get_node("Game")._on_died_enemy(self)
	
	var ex = ResourceLoader.load("res://Explosion.scn").instance()
	ex.set_pos(get_pos())
	get_tree().get_root().get_node("Game/Wave").add_child(ex)
	
	var ex = ResourceLoader.load("res://Trace.scn").instance()
	ex.set_pos(get_pos())
	get_tree().get_root().get_node("Game/Wave").add_child(ex)
	
	
	queue_free()

func hurt(atk):
	var v = atk-def
	if v<0:
		v=0
	life = life-v
	get_node("ProgressBar").set_value(life)
	if (life<=0):
		die()

