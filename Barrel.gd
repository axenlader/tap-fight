
extends KinematicBody2D

# member variables here, example:
# var a=2
# var b="textvar"
var life = 100
var ready = false
func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	get_node("ProgressBar").set_value(life)

func die():
	queue_free()

func hurt(atk):
	life = life-atk
	get_node("ProgressBar").set_value(life)
	if (life<=0):
		die()