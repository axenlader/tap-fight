
extends Area2D

# member variables here, example:
# var a=2
# var b="textvar"
var visible = []
var time = 0
var fire_delay = 1
var next_fire_time=0
var life = 100
var ready = false
var factor=0.0

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	get_node("ProgressBar").set_value(life)
	set_fixed_process(true)
	update_price()


func hurt(atk):
	life = life-atk
	get_node("ProgressBar").set_value(life)
	if (life<=0):
		queue_free()

func _on_Tower_body_enter( body ):
	if body.is_in_group("Enemy"):
		visible.append(body)
	
func _fixed_process(delta):
	time = time+delta
	if !visible.empty():
		get_node("Canon").look_at(visible[0].get_pos())
		try_fire(visible[0])

func try_fire(body):
	if time>=next_fire_time:
		shot(body)
		var d = fire_delay
		if factor>0:
			d = d/(2.0*factor)
		next_fire_time=time+d+0.1

func _on_Tower_body_exit( body ):
	visible.erase(body)

func shot(target):
	var tr = ResourceLoader.load("res://Bullet.scn").instance()
	get_tree().get_root().add_child(tr)
	tr.set_pos(get_node("Canon").get_global_pos())
	tr.look_at(target.get_pos())


func _on_pressed():
	if ready:
		if get_node("UpgradeLayer").is_visible():
			get_node("UpgradeLayer").hide()
		else:
			get_node("UpgradeLayer").show()
	else:
		ready=true


func _on_Upgrade_released():
	pass

func _on_Button_toggled( pressed ):
	if !pressed:
		fire_delay=fire_delay/2.0
		print(str(fire_delay,"ppp"))

func get_price():
	return 100+factor*factor*10

func _on_Button_pressed():
	if get_tree().get_root().get_node("Game").try_buy(get_price()):
		factor=factor+1
		update_price()

func update_price():
	get_node("UpgradeLayer/Button").set_text(str("Upgrade (",get_price(),"$)"))
