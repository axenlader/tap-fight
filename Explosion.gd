
extends AnimatedSprite

# member variables here, example:
# var a=2
# var b="textvar"
var frame = 0
var delay = 0.01
var next_frame_time=0
var time=0

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	set_fixed_process(true)
	
func _fixed_process(delta):
	time=time+delta
	if (next_frame_time<time):
		next_frame_time=time+delay
		frame = (frame+1)
		if frame>=22:
			queue_free()
		else:
			set_frame(frame)


