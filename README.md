# README #

Tap Fight is a simple defense game made with Godot Engine. 


### How do I get set up? ###

Just clone the repository and open the engine.cfg file using Godot.

### Screenshots

![ss1.png](https://bitbucket.org/repo/7kpXyL/images/3157105982-ss1.png)
![ss2.png](https://bitbucket.org/repo/7kpXyL/images/1822302689-ss2.png)
![ss3.png](https://bitbucket.org/repo/7kpXyL/images/3877554628-ss3.png)